#!/usr/bin/env bash

# Copyright 2017 by Idiap Research Institute, http://www.idiap.ch
#
# Author(s):
#   Bastian Schnell, 17.07.17
#

usage() {
cat <<- EOF
    usage: $PROGNAME [OPTIONS] <input_file> <output_file>
    
    This program converts full HTK labels to Audacity-readable labels of the form:
    <start_sec> <end_sec> <label_name>
    Audacity expects an txt file so the output file should have txt extension.

    OPTIONS:
        -h                        show this help

    
    Examples:
        $PROGNAME doriangray_01_00010.lab doriangray_01_00010.txt
EOF
}

###############################
# Default options and functions
#
# set -o xtrace # Prints every command before running it, same as "set -x".
set -o errexit # Exit when a command fails, same as "set -e".
               # Use "|| true" for those who are allowed to fail.
               # Disable (set +e) this mode if you want to know a nonzero return value.
set -o pipefail # Catch mysqldump fails.
set -o nounset # Exit when using undeclared variables, same as "set -u".
set -o noclobber # Prevents the bash shell from overwriting files, but you can force it with ">|".
export SHELLOPTS # Used to pass above shell options to any called subscripts.

readonly PROGNAME=$(basename $0)
readonly PROGDIR=$(readlink -m $(dirname $0))
readonly ARGS="$@"

# Provide log function. Use source bash_logging.sh if file exists in current PATH variable.
# source does not work with -u option, so disable it temporarily.
# set +u
# source bash_logging.sh
# set -u
log()
{
    echo -e >&2 "$@"
}

# Die should be called when an error occurs with a HELPFUL error message.
die () {
    log "ERROR" "$@"
    exit 1
}


# Set magic variables for current file & directory (upper-case).

# The main function of this file.
main()
{
    log "INFO" "Run ${PROGNAME} $@"
    
    # Read flags and optional parameters.
    while getopts ":habf:v-:" flag; do # If a character is followed by a colon (e.g. f:), that option is expected to have an argument.
        case "${flag}" in
            h) usage; exit ;;
            \?) die "Invalid option: -$OPTARG" ;;
        esac
    done
    shift $(($OPTIND - 1)) # Skip the already processed arguments.
    
    # Read arguments.
    local expectedArgs=2 # Always use "local" for variables, global variables are evil anyway.
    if [[ $# != "${expectedArgs}" ]]; then
        usage # Function call.
        die "Wrong number of parameters, expected ${expectedArgs} but got $#."
    fi    
    # Read parameters, use default values (:-) to work with -u option.
    local input_file=${1:-}
    local output_file=${2:-}
    
    ########################
    # Main functionality of this file.
    #
    
    # awk -F '[ +-]' '{print $1/10000000"\t"$2/10000000"\t"$4}' "${input_file}" >| "${output_file}"
    
    # The following for state alignment.
    printf %s "$(< ${input_file})" | awk -F  ' ' 'BEGIN{ RS="[[6]]" } {print $1/10000000" "$(NF-1)/10000000" "$3}' | awk -F '[ +-]' '{print $1"\t"$2"\t"$4}' >| "${output_file}"

}

# Call the main function, provide all parameters.
main "$@"
