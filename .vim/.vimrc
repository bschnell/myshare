packloadall
silent! helptags ALL
syntax on
filetype plugin indent on
set encoding=utf-8
set clipboard=unnamedplus
inoremap jk <ESC>
set spell spelllang=en_us"test"
vnoremap . :norm.

"split navigations
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

"enter newlines without entering insert mode
"S-Enter might not work in all consoles use instead
"nnoremap <S-C-O> O<ESC>
nnoremap <S-Enter> O<ESC>
nnoremap <CR> o<ESC>

map <up> <nop>
map <down> <nop>
map <left> <nop>
map <right> <nop>