#!/usr/bin/env python
# -*- coding: utf-8 -*-

# In-depth Description:
#

__author__ = "Bastian Schnell"
__version__ = "Revision: 1.0"
__date__ = "Date: 2018/01/01"
__copyright__ = "Copyright (c) 2018 by Idiap Research Institute, http://www.idiap.ch"
# __license__ = "..."

usage = """Module description.

Usage::

    >>> import ClassName
    >>> example...

:param p1: Description of p1.
:rtype: Description of return value.
"""

# System imports.
import logging
import argparse

# Third-party imports.

# Local source tree imports.


# class CapWords(object):
#     """Class description.
#     """
#     logger = logging.getLogger(__name__)

#     # Constants.
#     ALL_CAPS_WITH_UNDERSCORES = 1.0

#     ########################
#     # Default constructor
#     #
#     def __init__(self):
#         """Default constructor description.
#         """
#         self.logger = logger or logging.getLogger(__name__)
#         self.debug = False

#     ########################
#     # Getter and setters
#     #
#     def setDebugMode(self, debug):
#         self.debug = debug


def lower_case_with_underscores():
    """One-line docstring for obvious functions."""
    print "do something obvious"


def main():
    parser = argparse.ArgumentParser(description=__doc__, formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-a", "--aparam", help="param description", type=str,
                        dest="aparam", required=True)
    parser.add_argument("-b", "--bflag", help="bflag description", default=False,
                        dest="bflag", action="store_true")

    # Parse arguments
    args = parser.parse_args()
    param1 = args.aparam

    # Main functionality
    print args.bflag


if __name__ == "__main__":
    main()
