#!/usr/bin/env bash

# Copyright 2017 by Idiap Research Institute, http://www.idiap.ch
#
# Author(s):
#   Bastian Schnell, DATE
#
# In-depth Description:
#
#

usage() {
cat <<- EOF
    usage: $PROGNAME [OPTIONS] <arg1>
    
    Program does...

    OPTIONS:
        -a --aflag                set aflag
        -b --bflag                set bflag
        -f --file <file>          set file
        -v --verbos               set verbos
        -h                        show this help

    
    Examples:
        Run with aflag option:
        $PROGNAME -a arg1
        Run with specific file:
        $PROGNAME -f file.txt arg1
EOF
}

###############################
# Default options and functions
#
# set -o xtrace # Prints every command before running it, same as "set -x".
set -o errexit # Exit when a command fails, same as "set -e".
               # Use "|| true" for those who are allowed to fail.
               # Disable (set +e) this mode if you want to know a nonzero return value.
set -o pipefail # Catch mysqldump fails.
set -o nounset # Exit when using undeclared variables, same as "set -u".
set -o noclobber # Prevents the bash shell from overwriting files, but you can force it with ">|".
export SHELLOPTS # Used to pass above shell options to any called subscripts.

readonly PROGNAME=$(basename $0)
readonly PROGDIR=$(readlink -m $(dirname $0))
readonly ARGS="$@"

# Provide log function. Use source bash_logging.sh if file exists in current PATH variable.
# source does not work with -u option, so disable it temporarily.
# set +u
# source bash_logging.sh
# set -u
log()
{
    echo -e >&2 "$@"
}

# This function can be used to selectively enable/disable debugging.
# Use with the set command to debug sections of the script.
DEBUG()
{       
    # Check to see if the enable debugging variable is set
    if [ -n "${DEBUG_ENABLE+x}" ]
    then                
        # Run whatever command/option/argument combo that was
        # passed to our DEBUG function.
        $@             
    fi
}

# Clean up is called by the trap at any stop/exit of the script.
cleanup()
{
    # Cleanup code.
    rm -rf "$TMP"
}
# Die should be called when an error occurs with a HELPFUL error message.
die () {
    log "ERROR" "$@"
    exit 1
}


# Set magic variables for current file & directory (upper-case).
readonly TMP=$(mktemp -d)

# Additional function used by main.
side_function() {
    printf "${FUNCNAME} $@\n" # Start every side function with printing the name and args.
}

# The main function of this file.
main()
{
    # Force execution of cleanup at the end.
    trap cleanup EXIT INT TERM HUP

    log "INFO" "Run ${PROGNAME} $@"
    
    # Read flags and optional parameters.
    local aflag='' # You can specify default parameters here.
    local bflag=''
    local files=''
    local verbose='false'
    while getopts ":habf:v-:" flag; do # If a character is followed by a colon (e.g. f:), that option is expected to have an argument.
        case "${flag}" in
            -) case "${OPTARG}" in
                   aflag) aflag='true' ;;
                   bflag) bflag='true' ;;
                   file) val="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
                           file="${val}" ;;
                   verbose) verbose='true' ;;
                   *) die "Invalid option: --${OPTARG}" ;;
               esac;;
            h) usage; exit ;;
            a) aflag='true' ;;
            b) bflag='true' ;;
            f) file="${OPTARG}" ;;
            v) verbos='true' ;;
            \?) die "Invalid option: -$OPTARG" ;;
            :)  die "Option -$OPTARG requires an argument." ;;
        esac
    done
    shift $(($OPTIND - 1)) # Skip the already processed arguments.
    
    # Read arguments.
    local expectedArgs=1 # Always use "local" for variables, global variables are evil anyway.
    if [[ $# != "${expectedArgs}" ]]; then
        usage # Function call.
        die "Wrong number of parameters, expected ${expectedArgs} but got $#."
    fi    
    # Read parameters, use default values (:-) to work with -u option.
    local arg1=${1:-}
    
    ##################################
    # Main functionality of this file.
    #
    side_function

    if [ "${verbose}" == 'true' ]; then
        log "Verbose is enabled."
    fi
}

# Call the main function, provide all parameters.
main "$@"
